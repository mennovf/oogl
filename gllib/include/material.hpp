#ifndef SHADER_HPP
#define SHADER_HPP
#include <gl/glew.h>
#include <SFML/OpenGL.hpp>
#include <string>
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "texture.hpp"
#include <unordered_map>
#include "ubo.hpp"
#include "glwrapper.hpp"
#include <memory>

class Material : public GLWrapper{
public:
    typedef std::shared_ptr < Material > Ptr;
    enum Type{
        VERTEX = 1 << 1,
        FRAGMENT = 1 << 2,
        TESS_CONTROL = 1 << 3,
        TESS_EVAL = 1 << 4,
        GEOMETRY = 1 << 5
    };
    Material() = delete;
    Material(const std::string& name, int type = Type::VERTEX | Type::FRAGMENT);
    virtual ~Material () = default;
    virtual void bind() override;

    GLuint getShader() const;
    void setParameter(const std::string& handle, const glm::vec3& v);
    void setParameter(const std::string& handle, const glm::vec2& v);
    void setParameter(const std::string& handle, const glm::mat4& M);
    void setParameter(const std::string& handle, const float p);
    void setParameter(const std::string& handle, const Ubo& ubo);

    void setTexture(int, Texture::Ptr);
protected:
    std::unordered_map<GLenum, Texture::Ptr> _textures;
    GLuint loadShader(const std::string& filename, GLenum type);
    bool hasType(int types, Type type) const;
    void createShaderProgram(const std::string& name, int types);
};


#endif /* end of include guard: SHADER_HPP */
