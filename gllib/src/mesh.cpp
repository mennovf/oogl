#include "mesh.hpp"
#include <cstring>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <iostream>


Mesh::Mesh(GLenum mode):
    GLWrapper{glBindVertexArray, glGenVertexArrays, glDeleteVertexArrays},
    _mode{mode},
    _verts{-1}{ }

Mesh::Mesh(const std::string& file):
    Mesh{}{
        loadFromFile(file);
    }

void Mesh::setAttrFromMemory(Attr attr, const std::vector<float>& vec, unsigned int amount){
    if (_verts == -1){
        _verts = vec.size() / amount;
    }
    if (vec.size() / amount != static_cast<unsigned>(_verts)){
        std::cerr << "error setting attr: " << attr << std::endl;
    }
    GLuint attrib = static_cast<GLuint>(attr);
    auto guard = _res->scopedBind();

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vec.size() * sizeof(float), vec.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(attrib, amount, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(attrib);
}

bool Mesh::loadFromFile(const std::string& fileName){
    auto importer = Assimp::Importer{};
    const aiScene* scene = importer.ReadFile(fileName, aiProcess_Triangulate | aiProcess_GenSmoothNormals);
    if (! scene || !scene->HasMeshes()){
        std::cerr << "Error: reading mesh " << fileName << " " << importer.GetErrorString() << std::endl; 
        return false;
    }
    const aiMesh* mesh = scene->mMeshes[0];
    loadMesh(mesh);
    return true;
}


GLuint Mesh::getVao() const{
    return _res->getID();
}

int Mesh::getVerts(){
    return _verts;
}

GLenum Mesh::getMode() const{
    return _mode;
}

void Mesh::setMode(GLenum m){
    _mode = m;
}

void Mesh::loadMesh(const aiMesh* mesh){
    std::printf("  %i vertices in mesh\n", mesh->mNumVertices);

    _verts = mesh->mNumVertices;
    auto guard = _res->scopedBind();

    std::vector<GLfloat> points;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texCoords;

    if (mesh->HasPositions()) {
        std::cout << "Mesh has positions" << std::endl;
        points.resize(_verts * 3);
        for (int i = 0; i < _verts; i++) {
            const aiVector3D vp = mesh->mVertices[i];
            points[i * 3] = (GLfloat)vp.x;
            points[i * 3 + 1] = (GLfloat)vp.y;
            points[i * 3 + 2] = (GLfloat)vp.z;
        }
    }
    if (mesh->HasNormals()) {
        std::cout << "Mesh has normals" << std::endl;
        normals.resize(_verts * 3);
        for (int i = 0; i < _verts; i++) {
            const aiVector3D vn = mesh->mNormals[i];
            normals[i * 3] = (GLfloat)vn.x;
            normals[i * 3 + 1] = (GLfloat)vn.y;
            normals[i * 3 + 2] = (GLfloat)vn.z;
        }
    }
    if (mesh->HasTextureCoords(0)) {
        std::cout << "Mesh has texcoords" << std::endl;
        texCoords.resize(_verts * 2);
        for (int i = 0; i < _verts; i++) {
            const aiVector3D vt = mesh->mTextureCoords[0][i];
            texCoords[i * 2] = (GLfloat)vt.x;
            texCoords[i * 2 + 1] = (GLfloat)vt.y;
        }
    }

    //load into vbo's
    if (mesh->HasPositions()) {
        GLuint vbo;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(
            GL_ARRAY_BUFFER,
            3 * _verts * sizeof(GLfloat),
            points.data(),
            GL_STATIC_DRAW
            );
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(0);
    }
    if (mesh->HasNormals()) {
        GLuint vbo;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(
            GL_ARRAY_BUFFER,
            3 * _verts * sizeof(GLfloat),
            normals.data(),
            GL_STATIC_DRAW
            );
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(1);
    }
    if (mesh->HasTextureCoords(0)) {
        GLuint vbo;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(
            GL_ARRAY_BUFFER,
            2 * _verts * sizeof(GLfloat),
            texCoords.data(),
            GL_STATIC_DRAW
            );
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(2);
    }
    if (mesh->HasTangentsAndBitangents()) {
        // NB: could store/print tangents here
    }

    std::printf("mesh loaded\n");
}
