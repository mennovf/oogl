#include "framebuffer.hpp"
#include <utility>
#include <map>
#include <iostream>
#include <functional>
#include <vector>

namespace{
    struct Nums{
        GLenum type, attachment;
    };
    const std::map<FrameBuffer::Type, Nums> GLNAMES = {{FrameBuffer::Type::DEPTH, {GL_DEPTH_COMPONENT, GL_DEPTH_ATTACHMENT}},
                                                       {FrameBuffer::Type::COLOR, {GL_RGBA, GL_COLOR_ATTACHMENT0}}};
}

FrameBuffer::FrameBuffer():
    GLWrapper(),
    _buf{0},
    _texs{},
    _viewportSize{}{
    }

FrameBuffer::FrameBuffer(sf::Vector2u size, bool enableDepth):
    GLWrapper(std::bind(glBindFramebuffer, GL_FRAMEBUFFER, std::placeholders::_1), glGenFramebuffers, glDeleteFramebuffers),
    _buf{0},
    _texs{},
    _viewportSize{size}{
        if (enableDepth){
            addRenderBuffer(size, Type::DEPTH);
        }
    }

void FrameBuffer::addRenderBuffer(sf::Vector2u size, FrameBuffer::Type type){
    auto bindGuard = _res->scopedBind();
    GLuint rb = 0;
    glGenRenderbuffers (1, &rb);
    glBindRenderbuffer (GL_RENDERBUFFER, rb);
    glRenderbufferStorage ( GL_RENDERBUFFER, GLNAMES.at(type).type, size.x, size.y);
    glFramebufferRenderbuffer ( GL_FRAMEBUFFER, GLNAMES.at(type).attachment, GL_RENDERBUFFER, rb);
}

void FrameBuffer::setTexture(Type type, Texture::Ptr tex, bool fromShader){
    if (fromShader){
        _texs.insert(std::make_pair(type, tex));
    }
    auto bindGuard = _res->scopedBind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GLNAMES.at(type).attachment, GL_TEXTURE_2D, tex->getTexture(), 0);
}

void FrameBuffer::enableDrawBufs(){
    auto bindGuard = _res->scopedBind();
    std::vector<GLenum> drawBufs{};
    for (const auto& p : _texs){
        drawBufs.push_back(GLNAMES.at(p.first).attachment);
    }
    glDrawBuffers (drawBufs.size(), drawBufs.data());
}

void FrameBuffer::disableDrawBufs(){
    auto bindGuard = _res->scopedBind();
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
}

bool FrameBuffer::isFinished(){
    auto bindGuard = _res->scopedBind();
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    return status == GL_FRAMEBUFFER_COMPLETE;
}

GLuint FrameBuffer::getBuf(){
    return _res->getID();
}

void FrameBuffer::viewport(){
    glViewport(0, 0, _viewportSize.x, _viewportSize.y);
}

sf::Vector2u FrameBuffer::viewportSize(){
    return _viewportSize;
}

void FrameBuffer::ViewportSize(sf::Vector2u s){
    _viewportSize = s;
}

void FrameBuffer::bind(){
    GLWrapper::bind();
    viewport();
}
