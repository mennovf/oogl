#include "eventHandler.hpp"
EventHandler::EventHandler() :  prevMousePos{}, m_cLMB{ false }{
}

void EventHandler::handleEvent(sf::Event event){
    switch (event.type){
        case (sf::Event::Closed): handleClose(event); break;
        case (sf::Event::MouseButtonPressed): handleClick(event); break;
        case (sf::Event::MouseButtonReleased): handleRelease(event); break;
        case (sf::Event::Resized): handleResize(event); break;
        case (sf::Event::GainedFocus): handleGainedFocus(event); break;
        case (sf::Event::LostFocus) : handleLostFocus(event); break;
        case (sf::Event::MouseMoved): mouseMovedHelper(event); break;
        case (sf::Event::KeyPressed): handleKeyPress(event); break;
        case (sf::Event::KeyReleased): handleKeyRelease(event); break;
        case (sf::Event::MouseWheelMoved): handleScroll(event); break;
        default: break;
    }
}

void EventHandler::handleClick(sf::Event event){
    switch(event.mouseButton.button){
        case sf::Mouse::Left: leftClickHelper(event); break;
        case sf::Mouse::Right: handleRightClick(event); break;
        case sf::Mouse::Middle: handleMiddleClick(event); break;
        default: break;
    }
}

void EventHandler::handleRelease(sf::Event event){
    switch(event.mouseButton.button){
        case sf::Mouse::Left: leftReleaseHelper(event); break;
        case sf::Mouse::Right: handleRightRelease(event); break;
        case sf::Mouse::Middle: handleMiddleRelease(event); break;
        default: break;
    }
}

void EventHandler::leftClickHelper(sf::Event event){
    m_cLMB = true;
    prevMousePos.x = event.mouseButton.x;
    prevMousePos.y = event.mouseButton.y;
    handleLeftClick(event);
}

void EventHandler::leftReleaseHelper(sf::Event event){
    m_cLMB = false;
    handleLeftRelease(event);
}

void EventHandler::mouseMovedHelper(sf::Event event){
    handleMouseMove(event);
    if(sf::Mouse::isButtonPressed(sf::Mouse::Right)){
        handleRightDrag(event);
    }
    if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
        handleLeftDrag(event);
    }

    prevMousePos.x = event.mouseMove.x;
    prevMousePos.y = event.mouseMove.y;
}
