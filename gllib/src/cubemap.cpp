#include "cubemap.hpp"
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <array>
#include <utility>

namespace{
    struct Pair{
        GLenum target;
        std::string fileName;
    };

    std::array<Pair, 6> faces{ {
        { GL_TEXTURE_CUBE_MAP_POSITIVE_X, "posx" },
        { GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "negx" },
        { GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "posy" },
        { GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "negy" },
        { GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "posz" },
        { GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "negz" }
        } };
}

CubeMap::CubeMap():
    Texture{GL_TEXTURE_CUBE_MAP}{}


void CubeMap::loadFromFile(const std::string& folder){
    auto guard = _res->scopedBind();
    for (auto pair : faces){
        auto path = folder + "/" + pair.fileName + ".jpg"; 
        auto img = getImage(path);
        auto dims = img.getSize();
        _size = dims;
        glTexImage2D(pair.target, 0, GL_RGBA, dims.x, dims.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.getPixelsPtr());
    }

    setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    setParameter(GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
