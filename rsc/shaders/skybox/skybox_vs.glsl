#version 420
layout(location = 0) in vec3 pos;

layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};

layout(std140) uniform Light{
    vec3 pos;
    vec3 dir;
    vec3 a;
    vec3 d;
    vec3 s;
    mat4 PV;
    float near;
    float far;
    float angle;
} light[3];

uniform mat4 M;

out vec3 texcoords;
void main(){
    vec3 worldCoord = (M * vec4(pos, 1.0)).xyz;
    texcoords = worldCoord;
    gl_Position = P * vec4((V * vec4(worldCoord, 0.0)).xyz, 1.0);
}
