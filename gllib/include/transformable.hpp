#ifndef TRANSFORMABLE_HPP
#define TRANSFORMABLE_HPP
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <memory>
#include "transform.hpp"

class Transformable
{
public:
    typedef std::shared_ptr<Transformable> Ptr;
    Transformable ();
    virtual ~Transformable () = default;

    void rotate(glm::mat4 R);
    void lookAt(glm::vec3);
    void move(glm::vec3 dl);

    void setPos(glm::vec3);
    glm::vec3 getPos() const;

    void setScale(glm::vec3);
    void setPreScale(glm::vec3);
    glm::vec3 getPreScale() const;
    glm::mat4 getTransformMatrix() const;

    Transform::Ptr getTransform() const;
    void setTransform(Transform::Ptr);

    void setForward(glm::vec3);
    glm::vec3 getForward() const;
    glm::vec3 getUp() const;
    glm::vec3 getRight() const;
protected:
    Transform::Ptr _transform;
};


#endif /* end of include guard: TRANSFORMABLE_HPP */

