#include "state.hpp"
#include <iostream>
#include <memory>
#include "statestack.hpp"

State::Context::Context(std::unique_ptr<sf::RenderWindow> w, const std::string& str):
    window{std::move(w)},
    font{}{
        if (!font.loadFromFile(str)){
            std::cerr << "Unable to open font: " << str << std::endl;
        }
}

State::State(StateStack& stack, bool finalUpdate, bool finalInput, std::string name):
    _name(name),
    _stateStack(stack),
    _context(_stateStack.getContext()),
    _finalUpdate{finalUpdate},
    _finalInput{finalInput}{
        
    }

bool State::isFinalUpdate(){
    return _finalUpdate;
}

bool State::isFinalInput(){
    return _finalInput;
}
