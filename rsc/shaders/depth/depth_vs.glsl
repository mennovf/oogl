#version 400
layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 normal;

layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};
uniform mat4 M;

out vec3 pos_eye;
out vec3 normal_eye;

void main(){
    vec4 worldCoord = M * vec4(vertex_position, 1.0);
    pos_eye = (V * M * vec4(vertex_position, 1.0)).xyz;
    normal_eye = normalize((V * M * vec4(normal, 0.0)).xyz);
    gl_Position = P * V * worldCoord;

}
