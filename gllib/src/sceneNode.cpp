#include "sceneNode.hpp"

SceneNode::SceneNode():
    Transformable{},
    _children{}{
}

void SceneNode::draw(glm::mat4 PM) const{
    drawThis(PM);
    drawChildren(PM);
}

void SceneNode::drawChildren(glm::mat4 PM) const{
    const auto parentT = PM * getTransformMatrix();
    for (const auto& child : _children){
        child->draw(parentT);
    }
}

void SceneNode::addChild(Ptr child){
    _children.push_back(child);
}

void SceneNode::update(sf::Time){
}

void SceneNode::drawThis(glm::mat4) const{
}

std::vector<SceneNode::Ptr>& SceneNode::getChildren(){
    return _children;
}
