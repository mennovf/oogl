#ifndef FRAMEBUFFER_HPP
#define FRAMEBUFFER_HPP
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>
#include <map>
#include "texture.hpp"
#include "glwrapper.hpp"

class FrameBuffer: public GLWrapper{
public:
    enum Type{
        DEPTH,
        COLOR
    };
    FrameBuffer();
    FrameBuffer(sf::Vector2u, bool enableDepth = true);
    virtual ~FrameBuffer () = default;
    void setTexture(Type, Texture::Ptr, bool fromShader = true);
    void addRenderBuffer(sf::Vector2u, Type);
    void enableDrawBufs();
    void disableDrawBufs();
    bool isFinished();
    GLuint getBuf();
    void viewport();
    sf::Vector2u viewportSize();
    void ViewportSize(sf::Vector2u);

    virtual void bind() override;

protected:
    GLuint _buf;
    std::map<Type, Texture::Ptr> _texs;
    sf::Vector2u _viewportSize;

};


#endif /* end of include guard: FRAMEBUFFER_HPP */

