#version 400

layout (points) in;
layout (triangle_strip) out;
layout (max_vertices = 4) out;

layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};

uniform vec2 size = vec2(0.05, 0.05);

out vec2 vertexUV;

void main(){
    vec2 zw = gl_in[0].gl_Position.zw;
    vec2 pos = gl_in[0].gl_Position.xy;
    vec2 dir = size * vec2(1, 1);

    //Upper left
    gl_Position = P * vec4(pos + vec2(-1, 1) * dir, zw);
    vertexUV = vec2(0.0, 0.0);
    EmitVertex();  

    //Lower left 
    gl_Position = P * vec4(pos - dir, zw);
    vertexUV = vec2(0.0, 1.0);
    EmitVertex();  

    //Upper right 
    gl_Position = P * vec4(pos + dir, zw);
    vertexUV = vec2(1.0, 0.0);
    EmitVertex();  

    //Lower right 
    gl_Position = P * vec4(pos + vec2(1, -1) * dir, zw);
    vertexUV = vec2(1.0, 1.0);
    EmitVertex();  

    EndPrimitive();  
}
