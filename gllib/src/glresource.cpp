#include "glresource.hpp"
#include <SFML/Window.hpp>

GLResource::GLResource():
    GLResource(nullptr, nullptr, nullptr){
    }

GLResource::GLResource(BindFunc bind, GenFunc gen, DelFunc del):
    _bind_func{bind},
    _gen_func{gen},
    _delete_func{del},
    _id{0}{
    }

void GLResource::gen(){
    if (_gen_func){
        _gen_func(1, &_id);
    }
}

GLResource::~GLResource(){
    if (_delete_func){
        _delete_func(1, &_id);
    }
}

GLuint GLResource::getID(){
    return _id;
}

void GLResource::setID(GLuint id){
    _id = id;
}

void GLResource::bind(){
    if (_bind_func){
        _bind_func(_id);
    }
}

void GLResource::unbind(){
    if (_bind_func){
        _bind_func(0);
    }
}

GLResource::ScopedBind::ScopedBind(GLResource* res):
    _res(res){
        _res->bind();
    }

GLResource::ScopedBind::~ScopedBind(){
    if (_res){
        _res->unbind();
    }
}

GLResource::ScopedBind::ScopedBind(GLResource::ScopedBind&& other){
    _res = other._res;
    other._res = nullptr;
}

GLResource::ScopedBind GLResource::scopedBind(){
    return {this};
}
