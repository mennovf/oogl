#ifndef APP_HPP
#define APP_HPP
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <glm/glm.hpp>
#include "camera.hpp"
#include "material.hpp"
#include "model.hpp"
#include "sceneNode.hpp"
#include "texture2d.hpp"
#include "framebuffer.hpp"
#include "light.hpp"
#include <memory>
#include "ubo.hpp"
#include "state.hpp"
#include "initglew.hpp"

class App : public State{
public:
    App(StateStack&);

protected:
    typedef SceneNode SceneRoot;
    virtual void render() override;
    virtual void update(sf::Time dt) override;
    void close();
    void spawnSphere();

    void updateFps(float dt);
    void rotateCamera(float);
   

    InitGlew _initGlew;
    bool _running = true;
    sf::Clock _clock;

    SceneRoot _root;
    Light _light;
    Material::Ptr _modelShader;
    Material::Ptr _ppShader;
    Material::Ptr _floorShader;
    Ubo _cameraMatrices;
    std::shared_ptr<Model> _model;
    sf::Vector2i _prevMouse;

    FrameBuffer _fb;
    Texture2D::Ptr _fb_tex;
    FrameBuffer _depth_fb;
    Texture2D::Ptr _depth_tex;
    Texture2D::Ptr _camColor;
    Model _screenQuad;
    Mesh::Ptr _sphere;
    


    virtual void handleKeyRelease(sf::Event event) override;
    virtual void handleClose(sf::Event event) override;
    virtual void handleResize(sf::Event event) override;
    virtual void handleGainedFocus(sf::Event event) override;
    virtual void handleLostFocus(sf::Event event) override;
    virtual void handleLeftClick(sf::Event event) override;


    Camera _cam;
    float _fps;
    sf::Font _font;

    bool _focussed;
    void setupGLDebug();
};
#endif /* end of include guard: APP_HPPAPP_HPP */
