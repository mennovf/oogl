#include "camera.hpp"
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera():
    SceneNode{},
    _far{100.0f},
    _near{0.01f},
    _vFov{67.f},
    _aspect{4.0f/3.0f}{
    }

glm::mat4 Camera::getPerspective() const{
    return glm::perspective(3.141596f/180 * _vFov, _aspect, _near, _far);
}

glm::mat4 Camera::getView() const{
    return glm::lookAt(getPos(), getPos() + getForward(), getUp());   
}

glm::mat4 Camera::getPView() const{
    return getPerspective() * getView();
}

void Camera::setFar(float far){
    _far = far;
}

void Camera::setNear(float near){
    _near = near;
}

void Camera::setFov(float fov){
    _vFov = fov;
}

void Camera::setAspect(float aspect){
    _aspect = aspect;
}

float Camera::getFar() const{
    return _far;
}

float Camera::getNear() const{
    return _near;
}

float Camera::getFov() const{
    return _vFov;
}

float Camera::getAspect() const{
    return _aspect;
}
