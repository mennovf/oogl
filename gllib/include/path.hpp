#ifndef PATH_HPP
#define PATH_HPP
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include "transformable.hpp"
#include <vector>

class Path : public Transformable{
public:
    Path ();
    Path (const std::vector<glm::vec3>&, float);
    Path (const std::vector<glm::vec3>&, const std::vector<float>&);
    virtual ~Path () = default;
    void update(float);
    glm::vec3 getPosAlongPath() const;
    void reset();
    bool isDone() const;

protected:
    void calcMax();
    std::vector<glm::vec3> _points;
    std::vector<float> _ts;
    float _t;
    float _max;
};


#endif /* end of include guard: PATH_HPP */
