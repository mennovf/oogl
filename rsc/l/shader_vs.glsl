#version 440
layout(location = 0) in vec2 pos;
layout(location = 2) in vec2 texcoord_in;

out vec2 texcoord;

uniform mat4 M;

void main(){
    gl_Position = M * vec4(pos, 0, 1);
    texcoord = texcoord_in;
}
