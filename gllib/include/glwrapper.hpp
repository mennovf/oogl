#ifndef GLWRAPPER_HPP
#define GLWRAPPER_HPP
#include "glresource.hpp"
#include <memory>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>

class GLWrapper{
public:
    GLWrapper();
    GLWrapper(GLResource::BindFunc, GLResource::GenFunc, GLResource::DelFunc);
    virtual ~GLWrapper() = default;

    virtual void bind();
    virtual void unbind();
    GLResource::ScopedBind scopedBind();
    std::shared_ptr<GLResource> getRes();

protected:
    std::shared_ptr<GLResource> _res;

};


#endif /* end of include guard: GLWRAPPER_HPP */

