#include "glwrapper.hpp"
#include <utility>

GLWrapper::GLWrapper():
    GLWrapper(nullptr, nullptr, nullptr){}

GLWrapper::GLWrapper(GLResource::BindFunc bf, GLResource::GenFunc gf, GLResource::DelFunc df):
    _res{std::make_shared<GLResource>(bf, gf, df)}{
        _res->gen();
    }

void GLWrapper::bind(){
    _res->bind();
}

void GLWrapper::unbind(){
    _res->unbind();
}

std::shared_ptr<GLResource> GLWrapper::getRes(){
    return _res;
}

GLResource::ScopedBind GLWrapper::scopedBind(){
    return  std::move(_res->scopedBind());
}
