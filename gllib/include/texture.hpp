#ifndef TEXTURE_HPP
#define TEXTURE_HPP
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <memory>
#include "glwrapper.hpp"
#include <functional>

class Texture : public GLWrapper{
public:
    typedef std::shared_ptr<Texture> Ptr;
    Texture(GLenum target);
    virtual ~Texture() = default;

    virtual void loadFromFile(const std::string&) {};
    void setParameter(GLenum, GLint);

    GLuint getTexture() const;

    sf::Vector2u getSize() const;

    friend bool operator<(const Texture&, const Texture&);
protected:
    sf::Image getImage(const std::string&);
    GLenum _target;
    sf::Vector2u _size;
};


#endif /* end of include guard: TEXTURE_HPP */

