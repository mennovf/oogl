#include "model.hpp"
#include <glm/gtc/matrix_transform.hpp>

Model::Model():
    SceneNode{},
    _mesh{std::make_shared<Mesh>()},
    _shader{}{
}

Model::Model(const std::string& file):
    Model{}{
        _mesh->loadFromFile(file);
    }

Model::Model(const std::string& file, Material::Ptr shader):
    Model{file}{
        _shader = shader;
}

Model::Model(Mesh::Ptr mesh):
    Model{}{
        _mesh = mesh;
    }

Model::Model(Mesh::Ptr mesh, Material::Ptr shader) :
    Model{mesh}{
        _shader = shader;
    }

void Model::setMesh(Mesh::Ptr mesh){
    _mesh = mesh;
}

Mesh::Ptr& Model::getMesh(){
    return _mesh;
}

void Model::setShader(Material::Ptr s){
    _shader = s;
}

Material::Ptr Model::getShader(){
    return _shader;
}

void Model::drawThis(glm::mat4 PM) const{
    _shader->setParameter("M", PM * glm::scale(getTransformMatrix(), getPreScale()));
    _shader->bind();
    glBindVertexArray(_mesh->getVao());
    glDrawArrays(_mesh->getMode(), 0, _mesh->getVerts());
    glUseProgram(0);
}
