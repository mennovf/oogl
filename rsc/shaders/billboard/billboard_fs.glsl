#version 420
layout (binding = 1) uniform sampler2D tex;
in vec2 vertexUV;
out vec4 fragColour;

void main(){
    fragColour = texture(tex, vertexUV);
}
