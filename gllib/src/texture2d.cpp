#include "texture2d.hpp"
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>

Texture2D::Texture2D() :
    Texture(GL_TEXTURE_2D){
}

Texture2D::Texture2D(const std::string& name) :
    Texture2D{}{
    loadFromFile(name);
}

void Texture2D::loadFromFile(const std::string& name){
    auto img = getImage(name);
    genTexture(img.getPixelsPtr(), img.getSize());
}

void Texture2D::genEmpty(sf::Vector2u size, GLenum format, GLenum type){
    genTexture(NULL, size, format, type);
}

void Texture2D::genTexture(const sf::Uint8* adress, sf::Vector2u size, GLenum format, GLenum type){
    _size = size;
    auto guard = _res->scopedBind();
    glTexImage2D(GL_TEXTURE_2D, 0, format, size.x, size.y,
            0, format, type, adress);

    //default wrapping methods
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}