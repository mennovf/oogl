#ifndef STATE_HPP
#define STATE_HPP
#include <SFML/System.hpp>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <SFML/window.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <memory>
#include "eventHandler.hpp"

class StateStack;

class State : public EventHandler{
public:
    class Context
    {
    public:
        Context(std::unique_ptr<sf::RenderWindow>, const std::string&);
        std::unique_ptr<sf::RenderWindow> window;
        sf::Font font;
    };

    State(StateStack&, bool, bool, std::string);
    virtual ~State() = default;

    virtual void render(){};
    virtual void update(sf::Time){};
    virtual void input(sf::Event e){ handleEvent(e); };

    bool isFinalUpdate();
    bool isFinalInput();

    std::string getName() const { return _name; }
    void setName(std::string val) { _name = val; }

protected:
    std::string _name;
    StateStack& _stateStack;
    Context& _context;
    bool _finalUpdate;
    bool _finalInput;
};
#endif /* end of include guard: STATE_HPP */
