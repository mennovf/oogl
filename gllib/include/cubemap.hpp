#ifndef CUBEMAP_HPP
#define CUBEMAP_HPP
#include "texture.hpp"

class CubeMap : public Texture{
public:
    CubeMap();
    virtual ~CubeMap() = default;

    virtual void loadFromFile(const std::string&) override;

protected:

};
#endif