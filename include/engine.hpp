#ifndef ENGINE_HPP
#define ENGINE_HPP
#include "statestack.hpp"

class Engine : public StateStack{
public:
    Engine();
    virtual ~Engine() = default;
};


#endif /* end of include guard: ENGINE_HPP */

