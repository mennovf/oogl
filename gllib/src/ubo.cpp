#include "ubo.hpp"
#include <stdexcept>
#include <functional>


std::set<unsigned int> Ubo::_usedBindingIndices{0};

Ubo::Ubo():
    Ubo{0}{
    }

Ubo::Ubo(unsigned int size):
    GLWrapper{std::bind(glBindBuffer, GL_UNIFORM_BUFFER, std::placeholders::_1),
              glGenBuffers,
              [&](GLsizei amount, const GLuint* id){
                  glDeleteBuffers(amount, id);
                  _usedBindingIndices.erase(_bindingIndex);}},
    _size{size},
    _bindingIndex{0}{
        auto bindGuard = _res->scopedBind();
        glBufferData(GL_UNIFORM_BUFFER, size, NULL, GL_STREAM_DRAW);

        _bindingIndex = genBindingIndex();
        bindBufferBase();
    }

Ubo::Ubo(const std::vector<float>& ar):
    Ubo(ar.size()){
        auto bindGuard = _res->scopedBind();

        glBufferData(GL_UNIFORM_BUFFER, ar.size(), ar.data(), GL_STREAM_DRAW);
        bindBufferBase();
}

void Ubo::setData(int offset, const std::vector<float>& ar){
    auto bindGuard = _res->scopedBind();
    glBufferSubData(GL_UNIFORM_BUFFER, offset, ar.size(), ar.data());
}

void Ubo::setData(int offset, const glm::mat4& mat){
    auto bindGuard = _res->scopedBind();
    glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::mat4), &mat[0][0]);
}

void Ubo::setData(int offset, const float val){
    auto bindGuard = _res->scopedBind();
    glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(float), &val);
}

void Ubo::setData(int offset, const glm::vec3 vec){
    auto bindGuard = _res->scopedBind();
    glBufferSubData(GL_UNIFORM_BUFFER, offset, 4 * sizeof(float), &vec[0]);
}

unsigned int Ubo::genBindingIndex(){
    int maxBindingIndices;
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &maxBindingIndices);

    for (int i = 0; i < maxBindingIndices; ++i){
        if (_usedBindingIndices.find(i) == _usedBindingIndices.end()){
            _usedBindingIndices.insert(i);
            return i;
        }
    }
    throw std::out_of_range("No Ubo binding points left.");
}

unsigned int Ubo::getBindingIndex() const{
    return _bindingIndex;
}

void Ubo::bindBufferRange(int offset, int size){
    if (size == -1){
        size = _size;
    }
    auto bindGuard = _res->scopedBind();
    glBindBufferRange(GL_UNIFORM_BUFFER, _bindingIndex, getUbo(), offset, size);
}

GLuint Ubo::getUbo(){
    return _res->getID();
}

void Ubo::bindBufferBase(){
    auto bindGuard = _res->scopedBind();
    glBindBufferBase(GL_UNIFORM_BUFFER, getBindingIndex(), getUbo());
}
