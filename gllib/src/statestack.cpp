#include "statestack.hpp"
#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

StateStack::StateStack() :
    _context{std::unique_ptr<sf::RenderWindow>(new sf::RenderWindow{sf::VideoMode{640, 480, 32}, "OpenGL App", sf::Style::Default, sf::ContextSettings{32, 0, 4, 4, 3}}),
             "C:/Windows/Fonts/Arial.ttf"},
    _states{},
    _clock{},
    _commands{}{
}

void StateStack::update(){
    sf::Time dt = _clock.restart();
    for (auto it = _states.rbegin(); it != _states.rend(); ++it){
        (*it)->update(dt);
        if ((*it)->isFinalUpdate()){
            break;
        }
    }
}

void StateStack::render(){
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (auto& state : _states){
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        state->render();
    }
    _context.window->display();
}

void StateStack::input(sf::Event e){
    for (auto it = _states.rbegin(); it != _states.rend(); ++it){
        (*it)->input(e);
        if ((*it)->isFinalInput()){
            break;
        }
    }
}

void StateStack::push(std::unique_ptr<State> state){
    _states.push_back(std::move(state));
}

void StateStack::pop(){
    _states.pop_back();
}

State::Context& StateStack::getContext(){
    return _context;
}

void StateStack::run(){
    while (_states.size() != 0){
        sf::Event e;
        while (_context.window->pollEvent(e)){
            input(e);
        }
        update();
        render();
        execCommands();
    }
}

void StateStack::pushCommand(Command c){
    _commands.push(c);
}

void StateStack::execCommands(){
    const auto size = _commands.size();
    for (unsigned int i = 0; i < size; ++i){
        auto command = _commands.front();
        _commands.pop();

        command();
    }
}

void StateStack::removeState(std::string s){
    for (unsigned int i = 0; i < _states.size(); ++i){
        if (s == _states[i]->getName()){
            _states.resize(i);
            break;
        }
    }
}
