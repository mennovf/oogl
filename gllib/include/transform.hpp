#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <memory>

class Transform{
public:
    typedef std::shared_ptr<Transform> Ptr;
    Transform(glm::vec3, glm::vec3, glm::vec3, glm::vec3 s = glm::vec3{1}, glm::vec3 ps = glm::vec3{1});
    virtual ~Transform() = default;

    glm::vec3 _forward, _up;
    glm::vec3 _pos;
    glm::vec3 _preScale;
    glm::vec3 _scale;
};

#endif
