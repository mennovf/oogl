#version 400
layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoord;

layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};
layout(std140) uniform Light{
    vec3 pos;
    vec3 dir;
    vec3 a;
    vec3 d;
    vec3 s;
    mat4 PV;
    float near;
    float far;
    float angle;
} light[3];
uniform mat4 M;

out vec3 pos_eye;
out vec3 normal_eye;
out vec2 texcoord_vs;
out vec4 inLightCoord[3];
out vec4 worldCoord;
void main(){
    worldCoord = M * vec4(vertex_position, 1.0);
    pos_eye = (V * worldCoord).xyz;
    normal_eye = (V * M * vec4(normal, 0.0)).xyz;
    texcoord_vs = texcoord;
    gl_Position = P * V * worldCoord;

#pragma optionNV unroll all
    for (int i = 0; i < 3; ++i){
        inLightCoord[i] = light[i].PV * worldCoord;
    }
}
