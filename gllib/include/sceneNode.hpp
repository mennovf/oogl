#ifndef SCENENODE_HPP
#define SCENENODE_HPP
#include <SFML/System.hpp>
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include "transformable.hpp"


class SceneNode : public Transformable{
public:
    typedef std::shared_ptr<SceneNode> Ptr;
    SceneNode ();
    virtual ~SceneNode () = default;

    virtual void update(sf::Time);
    void draw(glm::mat4 PM = glm::mat4{1}) const;
    void addChild(std::shared_ptr<SceneNode>);
    std::vector<Ptr>& getChildren();

protected:
    virtual void drawThis(glm::mat4 PM = glm::mat4{1}) const;
    void drawChildren(glm::mat4) const;
    std::vector<Ptr> _children;
};


#endif /* end of include guard: SCENENODE_HPP */

