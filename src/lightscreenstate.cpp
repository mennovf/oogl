#include "lightscreenstate.hpp"
#include "texture.hpp"
#include "model.hpp"
#include "mesh.hpp"
#include "material.hpp"
#include <iostream>


VisLight::VisLight(StateStack& stck, std::shared_ptr<Texture> color, std::shared_ptr<Texture> depth):
    State{stck, false, false, "LightScreen"},
    _color{color},
    _depth{depth}{
        {
            auto quad = std::make_shared<Mesh>();
            quad->setAttrFromMemory(Mesh::Attr::POS, std::vector<float>{
                    0.25, 0.25,
                    -0.25, 0.25,
                    -0.25, -0.25,
                    -0.25, -0.25,
                    0.25, -0.25,
                    0.25, 0.25
                    }, 2);
            quad->setAttrFromMemory(Mesh::Attr::TEXCOORD, std::vector<float>{
                    1, 1,
                    0, 1,
                    0, 0,
                    0, 0,
                    1, 0,
                    1, 1}, 2);

            Material::Ptr cShader{ std::make_shared<Material>("rsc/l/shader")};
            cShader->setTexture(0, _color);
            auto colM = std::make_shared<Model>();
            colM->setMesh(quad);
            colM->setShader(cShader);
            colM->move({-0.75, 0.75, 0});

            Material::Ptr dShader(std::make_shared<Material>("rsc/l/shader"));
            dShader->setTexture(0, _depth);
            auto colD = std::make_shared<Model>();
            colD->setMesh(quad);
            colD->setShader(dShader);
            colD->move({-0.25, 0.75, 0});

            _root.addChild(colM);
            _root.addChild(colD);
        }
}

void VisLight::render(){
    _root.draw();
}
