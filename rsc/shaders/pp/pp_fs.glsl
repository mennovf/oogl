#version 420
in vec2 texcoord;
layout(binding = 0) uniform sampler2D orig;

layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};

out vec4 colour;
void main(){
    colour = vec4(vec3(texture(orig, texcoord)), 1.0);
}
