#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#include "model.hpp"
#include <string>

class Skybox : public Model{
public:
    Skybox(const std::string&, const std::string&);
    virtual ~Skybox() = default;
protected:
    virtual void drawThis(glm::mat4) const override;
};


#endif /* end of include guard: SKYBOX_HPP */

