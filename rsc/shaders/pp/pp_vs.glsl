#version 400
layout (location = 0) in vec2 pos;
layout (location = 2) in vec2 vt;

out vec2 texcoord;

void main(){
    texcoord = vt;
    gl_Position = vec4(pos, 0.0, 1.0);
}
