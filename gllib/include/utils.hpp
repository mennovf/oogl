#ifndef UTILS_HPP
#define UTILS_HPP
#include <string>

std::string readFile(const std::string& name);
#endif /* end of include guard: OPENGL_UTILS_HPP */

