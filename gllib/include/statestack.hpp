#ifndef STATESTACK_HPP
#define STATESTACK_HPP
#include "state.hpp"
#include <vector>
#include <SFML/System.hpp>
#include <memory>
#include <queue>
#include <utility>
#include <string>
#include <functional>

class StateStack{
public:
    typedef std::function<void()> Command;
    StateStack();
    virtual ~StateStack() = default;

    void update();
    void render();
    void input(sf::Event);
    void run();

    void push(std::unique_ptr<State> state);
    void pop();

    void pushCommand(Command);

    State::Context& getContext();

    void removeState(std::string);
protected:
    void execCommands();
    State::Context _context;
    std::vector<std::unique_ptr<State>> _states;
    sf::Clock _clock;
    std::queue<std::function<void()>> _commands;
};


#endif /* end of include guard: STATESTACK_HPP */

