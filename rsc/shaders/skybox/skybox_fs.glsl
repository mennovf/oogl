#version 420
layout(binding = 0) uniform samplerCube skyboxTexture;
in vec3 texcoords;
out vec4 fragColour;

void main(){
    fragColour = texture(skyboxTexture, texcoords);
}
