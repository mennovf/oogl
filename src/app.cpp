#include <SFML/System.hpp>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <iostream>

#include <string>
#include "app.hpp"
#include <cmath>
#include <algorithm>
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "lightscreenstate.hpp"
#include "statestack.hpp"
#include "skybox.hpp"
#include "cubemap.hpp"

namespace{
    const unsigned int DEPTH_BUFFER_DIM = 6400u;
    const sf::Vector2u DEPTH_BUFFER_SIZE{DEPTH_BUFFER_DIM, DEPTH_BUFFER_DIM};
}
App::App(StateStack& stack): 
    State{stack, true, true, "App"},
    _initGlew(),
    _running{true},
    _clock{},
    _root{},
    _light{3.141596f/10.0f, glm::vec3{0.1f}, glm::vec3{0.7f}, glm::vec3{1.0f}},
    _modelShader{std::make_shared<Material>("rsc/shaders/model/model")},
    _ppShader{ std::make_shared<Material>("rsc/shaders/pp/pp")},
    _floorShader{ std::make_shared<Material>("rsc/shaders/floor/floor")},
    _cameraMatrices{2 * sizeof(glm::mat4)},
    _model{std::make_shared<Model>()},
    _prevMouse{},
    _fb{_context.window->getSize()},
    _fb_tex{ std::make_shared<Texture2D>() },
    _depth_fb{DEPTH_BUFFER_SIZE, false},
    _depth_tex{ std::make_shared<Texture2D>() },
    _camColor{ std::make_shared<Texture2D>() },
    _screenQuad{},
    _cam{},
    _fps{0},
    _font{},
    _focussed{true},
    _sphere{std::make_shared<Mesh>()}{
        std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
            << ", Vendor: " << glGetString(GL_VENDOR)
            << ", Renderer: " << glGetString(GL_RENDERER)
            << ", GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
        setupGLDebug();
        _context.window->setVerticalSyncEnabled(true);
        _context.window->setMouseCursorVisible(false);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);


        //Load skybox
        auto skybox = std::make_shared<Skybox>("rsc/shaders/skybox/skybox", "rsc/textures/cubemaps/chapel");
        skybox->getShader()->setParameter("CameraMatrices", _cameraMatrices);
        _root.addChild(skybox);

        sf::Mouse::setPosition(sf::Vector2i{240, 320}, *(_context.window));

        _font.loadFromFile("C:/Windows/Fonts/Arial.ttf");

        _light.setPos(glm::vec3{1});
        _light.setForward(glm::vec3{0} - _light.getPos());
        _light.setNear(1);
        _light.setFar(5);

        _sphere->loadFromFile("rsc/mesh/sphere.obj");
        _floorShader->setTexture(1, std::make_shared<Texture2D>("rsc/wally.jpg"));


        _model->getMesh()->loadFromFile("rsc/mesh/dragon.obj");
        _model->move({0, -0.1, 0});
        _model->setPreScale(glm::vec3{0.02f});
        _model->setShader(_modelShader);

        //Add light model at the location of the light
        {
            auto lshader = std::make_shared<Material>("rsc/shaders/billboard/billboard", Material::Type::FRAGMENT | Material::Type::VERTEX | Material::Type::GEOMETRY);
            auto tex = std::make_shared<Texture2D>("rsc/bulb.png");
            lshader->setTexture(1, tex);
            lshader->setParameter("CameraMatrices", _cameraMatrices);
            auto texSize = tex->getSize();
            glm::vec2 tSize = glm::vec2(static_cast<float>(texSize.x), static_cast<float>(texSize.y));
            float maxDim = std::max(tSize.x, tSize.y);
            lshader->setParameter("size", 0.05f / maxDim * tSize);

            auto lmodel = std::make_shared<Model>();
            lmodel->getMesh()->setMode(GL_POINTS);
            lmodel->setShader(lshader);
            lmodel->move(glm::vec3{0, 0.2, 0});
            lmodel->setTransform(_light.getTransform());
            //single point, the point will be expanded into a plane in the geometry shader
            lmodel->getMesh()->setAttrFromMemory(Mesh::Attr::POS, std::vector<float>{0, 0, 0}, 3);
            _root.addChild(lmodel);
        }


        //Add a camera model
        {
            auto camera = std::make_shared<Model>("rsc/mesh/camera.obj", _modelShader);
            camera->setTransform(_cam.getTransform());
            camera->setPreScale(glm::vec3{0.001f});
            _root.addChild(camera);
        }

        //Add teapot rotating parented to the dragon
        {
            auto reflect = std::make_shared<Material>("rsc/shaders/reflective/reflective");
            reflect->setParameter("CameraMatrices", _cameraMatrices);
            reflect->setParameter("Light[0]", _light.getUbo());
            reflect->setTexture(0, _depth_tex);
            auto environment = std::make_shared<CubeMap>();
            environment->loadFromFile("rsc/textures/cubemaps/chapel");
            reflect->setTexture(1, environment);

            auto pot = std::make_shared<Model>("rsc/mesh/teapot.obj", reflect);
            pot->setScale({0.2, 0.2, 0.2});
            pot->rotate(glm::rotate(glm::mat4{}, -3.141596f / 2.f, glm::vec3{1, 0, 0}));
            pot->move({0, 0.2, 0.2});
            _model->addChild(pot);

            auto floor = std::make_shared<Model>();
            floor->move({0, -0.1, 0});
            floor->setShader(_floorShader);
            floor->getMesh()->setAttrFromMemory(Mesh::Attr::POS, std::vector<float>{
                    -1, 0, -1,
                    -1, 0, 1,
                    1, 0, 1,
                    1, 0, 1,
                    1, 0, -1,
                    -1, 0, -1
                    }, 3);
            floor->getMesh()->setAttrFromMemory(Mesh::Attr::NORMAL, std::vector<float>{
                    0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0}, 3);
            floor->getMesh()->setAttrFromMemory(Mesh::Attr::TEXCOORD, std::vector<float>{
                    0, 0,
                    0, 1,
                    1, 1,
                    1, 1,
                    1, 0,
                    0, 0}, 2);
            _root.addChild(floor);

        }

        _root.addChild(_model);

        _cameraMatrices.setData(sizeof(glm::mat4), _cam.getPerspective());

        _floorShader->setParameter("CameraMatrices", _cameraMatrices);
        _modelShader->setParameter("CameraMatrices", _cameraMatrices);

        _floorShader->setParameter("Light[0]", _light.getUbo());
        _modelShader->setParameter("Light[0]", _light.getUbo());

        //set up the frame buffer for post-processing effects
        _fb_tex->genEmpty(_context.window->getSize());

        _fb.setTexture(FrameBuffer::Type::COLOR, _fb_tex);
        _fb.enableDrawBufs();

        //depth buffer for the light
        _depth_tex->genEmpty(DEPTH_BUFFER_SIZE, GL_DEPTH_COMPONENT);
        
        _depth_fb.setTexture(FrameBuffer::Type::DEPTH, _depth_tex, false);

        _camColor->genEmpty(DEPTH_BUFFER_SIZE);
        _depth_fb.setTexture(FrameBuffer::Type::COLOR, _camColor, true);
        //_depth_fb.disableDrawBufs();
        _depth_fb.enableDrawBufs();

        //Make screen space quad
        // x,y vertex positions
        std::vector<float> ss_quad_pos{
            -1.0, -1.0,
            1.0, -1.0,
            1.0,  1.0,
            1.0,  1.0,
            -1.0,  1.0,
            -1.0, -1.0
        };
        // per-vertex texture coordinates
        std::vector<float> ss_quad_st{
            0.0, 0.0,
            1.0, 0.0,
            1.0, 1.0,
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0
        };

        _ppShader->setTexture(0, _fb_tex);
        _screenQuad.getMesh()->setAttrFromMemory(Mesh::Attr::POS, ss_quad_pos, 2);
        _screenQuad.getMesh()->setAttrFromMemory(Mesh::Attr::TEXCOORD, ss_quad_st, 2);
        _screenQuad.setShader(_ppShader);

        _stateStack.pushCommand([&](){_stateStack.push(std::unique_ptr<State>(new VisLight(_stateStack, _camColor, _depth_tex))); });
    }

void App::update(sf::Time dt){
    static bool added = false;
    const float forward_speed{1};
    const float side_speed{1};
    const float t = dt.asSeconds();
    const float vSpeed = 1.f;
    glm::vec3 forward = _cam.getForward();
    forward[1] = 0;
    const glm::vec3 dir = glm::normalize(forward);
    const glm::vec3 right = _cam.getRight();
    const glm::vec3 up{0, 1, 0};

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Z)){
        _cam.move(t * forward_speed * dir);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
        _cam.move(-t * forward_speed * dir);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Q)){
        _cam.move(-t * side_speed * right);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
        _cam.move(t * side_speed * right);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
        _cam.move(vSpeed * up * t);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)){
        _cam.move(-vSpeed * up * t);
    }
    rotateCamera(t);

    _model->rotate(glm::rotate(glm::mat4{}, t, glm::vec3{0, 1, 0}));
    _light.setPos(glm::rotateY(_light.getPos(), t/10.f));
    
    _light.setForward(glm::vec3{0} - _light.getPos());
    _light.updateUbo();
}


void App::render(){
    _depth_tex->setParameter(GL_TEXTURE_COMPARE_MODE, GL_NONE);
    _modelShader->setTexture(0, std::make_shared<Texture2D>());
    _floorShader->setTexture(0, std::make_shared<Texture2D>());
    _depth_fb.bind();
    _cameraMatrices.setData(0, _light.getView());
    _cameraMatrices.setData(sizeof(glm::mat4), _light.getPerspective());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    _root.draw();
    _depth_fb.unbind();

    _depth_tex->setParameter(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    _cameraMatrices.setData(0, _cam.getView());
    _cameraMatrices.setData(sizeof(glm::mat4), _cam.getPerspective());
    _fb.bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    _modelShader->setTexture(0, _depth_tex);
    _floorShader->setTexture(0, _depth_tex);
    const auto size = _context.window->getSize();
    _root.draw();
    _fb.unbind();

    glViewport(0, 0, size.x, size.y);
    _screenQuad.draw();
    _depth_tex->setParameter(GL_TEXTURE_COMPARE_MODE, GL_NONE);
}

void App::rotateCamera(float dt){
    const sf::Vector2i curMouse = sf::Mouse::getPosition(*(_context.window));
    if (!(curMouse == _prevMouse)){
        const sf::Vector2i center{320, 240};
        const sf::Vector2i dMouse = curMouse - center;
        const float speed = 3.141596f/20;

        if (_focussed){
            glm::mat4 R = glm::rotate(glm::mat4{}, -dMouse.x * dt * speed, glm::vec3{0, 1, 0});
            R = glm::rotate(R, -dMouse.y * dt * speed, _cam.getRight());
            _cam.rotate(R);
            sf::Mouse::setPosition(center, *(_context.window));
        }
    }
}

void App::updateFps(float dt){
    _fps = 1.0f / dt;
}

namespace{

    const char* sev_str[] = {
        "severity: HIGH",
        "severity: MEDIUM",
        "severity: LOW"
    };

    const char* source_str[] = {
        "source: API",
        "source: WINDOW SYSTEM",
        "source: SHADER COMPILER",
        "source: THIRD PARTY",
        "source: APPLICATION",
        "source: OTHER"
    };

    const char* type_str[] = {
        "type: ERROR",
        "type: DEPRECATED BEHAVIOUR",
        "type: UNDEFINED BEHAVIOUR",
        "type: PORTABILITY",
        "type: PERFORMANCE",
        "type: OTHER"
    };

    void debug_gl_callback (
            unsigned int source,
            unsigned int type,
            unsigned int id,
            unsigned int severity,
            int length,
            const char* message,
            void* userParam
            ) {
        int src_i = source - 0x8246;
        int typ_i = type - 0x824C;
        int sev_i = severity - 0x9146;
        fprintf (
                stderr,
                "%s %s id: %u %s length: %i %s userParam: %i\n",
                source_str[src_i],
                type_str[typ_i],
                id,
                sev_str[sev_i],
                length,
                message,
                *(int*)userParam
                );
    }
}

void App::setupGLDebug(){
    if (GLEW_KHR_debug) {
        int param = -1;
        printf ("KHR_debug extension found\n");
        glDebugMessageCallback ((GLDEBUGPROC)debug_gl_callback, &param);
        glEnable (GL_DEBUG_OUTPUT_SYNCHRONOUS);
        printf ("debug callback engaged\n");
    } else {
        printf ("KHR_debug extension NOT found\n");
    }
}

void App::spawnSphere(){
    float range = 1.f;
    auto pos = _cam.getPos() + range * glm::normalize(_cam.getForward());

    auto model = std::make_shared<Model>(_sphere);
    model->setPreScale(glm::vec3{ .1f });
    model->setPos(pos);
    model->setShader(_modelShader);
    _root.addChild(model);
}

void App::handleKeyRelease(sf::Event event){
    static bool lState = true;
    using sfK = sf::Keyboard;
    switch (event.key.code){
    case sfK::L:
        if (lState){ _stateStack.pushCommand([&](){_stateStack.removeState("LightScreen"); lState = false; }); }
        else { _stateStack.pushCommand([&](){_stateStack.push(std::unique_ptr<State>(new VisLight(_stateStack, _camColor, _depth_tex))); }); lState = true; };
        break;
    case sfK::Escape:
        close();
        break;
    default: break;
    }
}

void App::close(){
    _stateStack.pushCommand([&](){_stateStack.removeState(this->getName()); });
}

void App::handleClose(sf::Event event){
    close();
}

void App::handleResize(sf::Event event){
    glViewport(0, 0, event.size.width, event.size.height);
}

void App::handleGainedFocus(sf::Event event){
    _focussed = true;
}

void App::handleLostFocus(sf::Event event){
    _focussed = false;
}

void App::handleLeftClick(sf::Event event){
    spawnSphere();
}
