#ifndef MODEL_HPP
#define MODEL_HPP
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include "material.hpp"
#include "sceneNode.hpp"
#include "mesh.hpp"

class Model: public SceneNode{
public:
    Model();
    Model(const std::string& file);
    Model(const std::string& file, Material::Ptr shader);
    Model(Mesh::Ptr);
    Model(Mesh::Ptr, Material::Ptr);
    virtual ~Model () = default;

    void setMesh(Mesh::Ptr);
    Mesh::Ptr& getMesh();

    void setShader(Material::Ptr);
    Material::Ptr getShader();

protected:
    virtual void drawThis(glm::mat4) const override;
    Mesh::Ptr _mesh;
    mutable Material::Ptr _shader;
};


#endif /* end of include guard: MODEL_HPP */

