#include "skybox.hpp"
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <SFML/Opengl.hpp>
#include "material.hpp"
#include "cubemap.hpp"
#include <utility>


Skybox::Skybox(const std::string& shaderfiles, const std::string& textures): Model{}{
    auto mesh = getMesh();
    mesh->setAttrFromMemory(Mesh::Attr::POS, std::vector<float>{
            -1.0f,  1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            -1.0f,  1.0f, -1.0f,
            1.0f,  1.0f, -1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f,  1.0f
            }, 3);
    auto shader = std::make_shared<Material>(shaderfiles);
    auto texture = std::make_shared<CubeMap>();
    texture->loadFromFile(textures);
    shader->setTexture(0, texture);
    setShader(shader);
}

void Skybox::drawThis(glm::mat4 PM) const{
    glDepthMask(GL_FALSE);
    Model::drawThis(PM);
    glDepthMask(GL_TRUE);
}
