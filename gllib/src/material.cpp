#include "material.hpp"
#include <cstring>
#include <iostream>
#include <utility>
#include "utils.hpp"
#include <map>
#include <string>

namespace{
    void checkGlErrors(GLuint ShaderObject, GLenum what){
        GLint succes;
        if (what == GL_COMPILE_STATUS)
            glGetShaderiv(ShaderObject, what, &succes);
        else
            glGetProgramiv(ShaderObject, what, &succes);
        if (!succes){
            GLint blen = 0;	
            GLsizei slen = 0;

            if (what == GL_COMPILE_STATUS)
                glGetShaderiv(ShaderObject, GL_INFO_LOG_LENGTH , &blen);       
            else
                glGetProgramiv(ShaderObject, GL_INFO_LOG_LENGTH , &blen);       
            if (blen > 1)
            {
                GLchar* compiler_log = (GLchar*)malloc(blen);
                if (what == GL_COMPILE_STATUS){
                    glGetShaderInfoLog(ShaderObject, blen, &slen, compiler_log);
                    std::cout << "compiler_log:" << compiler_log << std::endl;
                }
                else{
                    glGetProgramInfoLog(ShaderObject, blen, &slen, compiler_log);
                    std::cout << "linker_log:" << compiler_log << std::endl;
                }
                free (compiler_log);
            }
        }        
    }

    struct ShaderInfo{
        std::string postfix;
        const GLenum glType;
    };

    const std::map<Material::Type, ShaderInfo> shaderinfos = {{Material::Type::VERTEX, {"vs", GL_VERTEX_SHADER}},
                                                            {Material::Type::FRAGMENT, {"fs", GL_FRAGMENT_SHADER}},
                                                            {Material::Type::TESS_CONTROL, {"tc", GL_TESS_CONTROL_SHADER}},
                                                            {Material::Type::TESS_EVAL, {"te", GL_TESS_EVALUATION_SHADER}},
                                                            {Material::Type::GEOMETRY, {"gm", GL_GEOMETRY_SHADER}}};
    }

Material::Material(const std::string& name, int type):
    GLWrapper{glUseProgram,
        [type](GLsizei, GLuint* shader){*shader = glCreateProgram();},
        [](GLsizei, const GLuint * shader){glDeleteProgram(*shader);}}{
    createShaderProgram(name, type);
}

GLuint Material::loadShader(const std::string& filename, GLenum type){
    std::string shader_content = readFile(filename);
    const char* shader_string = shader_content.c_str();
    GLuint shader = glCreateShader(type);

    glShaderSource(shader, 1, &shader_string, NULL);
    glCompileShader(shader);
    checkGlErrors(shader, GL_COMPILE_STATUS);
    return shader;
}

void Material::bind(){
    GLWrapper::bind();
    for (const auto& tex : _textures){
        glActiveTexture(GL_TEXTURE0 + tex.first);
        tex.second->bind();
    }
}

GLuint Material::getShader() const{
    return _res->getID();
}

bool Material::hasType(int types, Type type) const{
    return !!(types & type);
}

void Material::createShaderProgram(const std::string& name, int types = Type::VERTEX | Type::FRAGMENT){
    GLuint shaderProgram = getShader();

    for (const auto& shaderType : shaderinfos){
        if (hasType(types, shaderType.first)){
            GLuint subShader = loadShader(name + "_" + shaderType.second.postfix + ".glsl", shaderType.second.glType);
            glAttachShader(shaderProgram, subShader);
            glDeleteShader(subShader);
        }
    }


    glLinkProgram(shaderProgram);
    checkGlErrors(shaderProgram, GL_LINK_STATUS);
}

void Material::setParameter(const std::string& handle, const glm::vec3& v){
    GLuint shader = getShader();
    glProgramUniform3f(shader, glGetUniformLocation(shader, handle.c_str()), v.x, v.y, v.z);
}

void Material::setParameter(const std::string& handle, const glm::vec2& v){
    GLuint shader = getShader();
    glProgramUniform2f(shader, glGetUniformLocation(shader, handle.c_str()), v.x, v.y);
}

void Material::setParameter(const std::string& handle, const glm::mat4& M){
    GLuint shader = getShader();
    glProgramUniformMatrix4fv(shader, glGetUniformLocation(shader, handle.c_str()), 1, GL_FALSE, &M[0][0]);
}

void Material::setParameter(const std::string& handle, const float p){
    GLuint shader = getShader();
    glProgramUniform1f(shader, glGetUniformLocation(shader, handle.c_str()), p);
}

void Material::setTexture(int handle, Texture::Ptr tex){
    _textures[handle] = tex;
}

void Material::setParameter(const std::string& handle, const Ubo& ubo){
    GLuint shader = getShader();
    auto index = glGetUniformBlockIndex(shader, handle.c_str());
    glUniformBlockBinding(shader, index, ubo.getBindingIndex());
}
