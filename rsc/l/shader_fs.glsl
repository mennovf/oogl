#version 440

in vec2 texcoord;
layout(binding = 0) uniform sampler2D tex;

out vec4 colour;
void main(){
    colour = vec4(texture(tex, texcoord));
}
