#include "transformable.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

Transformable::Transformable():
    _transform{std::make_shared<Transform>(glm::vec3{0, 0, 0}, glm::vec3{0, 1, 0}, glm::vec3{0, 0, 1})}{
    }

void Transformable::rotate(glm::mat4 R){
    _transform->_forward = glm::vec3(R * glm::vec4(_transform->_forward, 0.f));
    _transform->_up = glm::vec3(R * glm::vec4(_transform->_up, 0.f));
}

void Transformable::lookAt(glm::vec3 t){
    _transform->_forward = glm::normalize(t - _transform->_pos);
}

void Transformable::setPreScale(glm::vec3 s){
    _transform->_preScale = s;
}

glm::vec3 Transformable::getPreScale() const{
    return _transform->_preScale;
}

void Transformable::setScale(glm::vec3 s){
    _transform->_scale = s;
}

void Transformable::move(glm::vec3 dl){
    _transform->_pos += dl;
}

void Transformable::setPos(glm::vec3 p){
    _transform->_pos = p;
}

glm::mat4 Transformable::getTransformMatrix() const{
    glm::mat4 T{1};
    T[0] = glm::vec4(-getRight(), 0.0);
    T[1] = glm::vec4(getUp(), 0.0);
    T[2] = glm::vec4(getForward(), 0.0);
    T[3] = glm::vec4(getPos(), 1.0);
    T = glm::scale(T, _transform->_scale);
    return T;
}

Transform::Ptr Transformable::getTransform() const{
    return _transform;
}

void Transformable::setTransform(Transform::Ptr t){
    _transform = t;
}

void Transformable::setForward(glm::vec3 vec){
    _transform->_forward = glm::normalize(vec);
}

glm::vec3 Transformable::getForward() const{
    return _transform->_forward;
}

glm::vec3 Transformable::getUp() const{
    return _transform->_up;
}

glm::vec3 Transformable::getPos() const{
    return _transform->_pos;
}

glm::vec3 Transformable::getRight() const{
    return glm::cross(getForward(), getUp());
}
