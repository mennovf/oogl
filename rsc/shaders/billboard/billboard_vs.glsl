#version 400
layout(location = 0) in vec3 pos;

layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};
uniform mat4 M;

void main(){
    gl_Position = V * M * vec4(pos, 1.0);
}
