cmake_minimum_required(VERSION 2.8)
project("OpenGL")

add_subdirectory(gllib)

set(EXECUTABLE_NAME Opengl)
set(CPPS
    main.cpp
    app.cpp
    engine.cpp
    lightscreenstate.cpp)

foreach(CPP ${CPPS})
    set(SOURCE ${SOURCE} "${CMAKE_SOURCE_DIR}/src/${CPP}")
endforeach()

include_directories(${CMAKE_SOURCE_DIR}/include)
include_directories(${CMAKE_SOURCE_DIR}/gllib/include)
add_executable(${EXECUTABLE_NAME} ${SOURCE})
target_link_libraries(${EXECUTABLE_NAME} oogl)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic")
#add_definitions("-pedantic -Wall -Wextra")

#Detect and add boost
#set(Boost_USE_STATIC_LIBS TRUE)
#find_package(Boost REQUIRED)
#if(Boost_FOUND)
    #include_directories(${Boost_INCLUDE_DIR})
#endif()
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/" ${CMAKE_MODULE_PATH})

find_package(GLM REQUIRED)
include_directories(${GLM_INCLUDE_DIRS})

#Detect and add SFML
find_package(SFML 2 REQUIRED system window graphics)
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(${EXECUTABLE_NAME} ${SFML_LIBRARIES})
endif()

file(COPY "${CMAKE_SOURCE_DIR}/rsc" DESTINATION "${CMAKE_BINARY_DIR}")

file(GLOB DLLS "${CMAKE_SOURCE_DIR}/lib/*.dll")
foreach(DLL ${DLLS})
    get_filename_component(HANDLE ${DLL} NAME)
endforeach()
