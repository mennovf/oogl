#include "utils.hpp"
#include <fstream>
#include <streambuf>

#include <cstdlib>
#include <iostream>
#include <sstream>

std::string readFile(const std::string& name){
    std::ifstream t(name);
    if (!t.is_open()){
        std::cerr << "error opening file: " << name << std::endl;
        return {};
    }
    return static_cast<std::ostringstream&>(std::ostringstream{} << t.rdbuf()).str();;
}
