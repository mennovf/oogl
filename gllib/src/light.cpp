#include "light.hpp"

namespace{
    enum UboOrder{
        POSITION = 0,
        DIRECTION,
        AMBIENT,
        DIFFUSE,
        SPECULAR,
        PV,
        BB_NEAR,
        BB_FAR,
        HALFANGLE
    };
    const unsigned int UBOSIZE = (5 * 4 + 16 + 3) * sizeof(GLfloat); //5 vectors, 1 matrix and 3 float
}


Light::Light():
    Light{3.141596f, glm::vec3{0}, glm::vec3{0}, glm::vec3{0}}{}

Light::Light(float angle, glm::vec3 amb, glm::vec3 diff, glm::vec3 spec):
    Camera{},
    _halfAngle{angle},
    _ambient{amb},
    _diffuse{diff},
    _specular{spec},
    _uboCreated{false},
    _ubo{}{}

void Light::setAmbient(glm::vec3 col){
    _ambient = col;
}

void Light::setDiffuse(glm::vec3 col){
    _diffuse = col;
}

void Light::setSpecular(glm::vec3 col){
    _specular = col;
}

glm::vec3 Light::getAmbient() const{
    return _ambient;
}

glm::vec3 Light::getDiffuse() const{
    return _diffuse;
}

glm::vec3 Light::getSpecular() const{
    return _specular;
}

void Light::setHalfAngle(float angle){
    _halfAngle = angle;
}

float Light::getHalfAngle() const{
    return _halfAngle;
}

Ubo& Light::getUbo() const{
    updateUbo();
    return _ubo;
}

void Light::updateUbo() const{
    if (!_uboCreated){
        _ubo = Ubo{UBOSIZE};
        _uboCreated = true;
    }
    //fill in the ubo with recent values
    _ubo.setData(UboOrder::POSITION * 4 * sizeof(GLfloat), getPos());
    _ubo.setData(UboOrder::DIRECTION * 4 * sizeof(GLfloat), getForward());
    _ubo.setData(UboOrder::AMBIENT * 4 * sizeof(GLfloat), _ambient);
    _ubo.setData(UboOrder::DIFFUSE * 4 * sizeof(GLfloat), _diffuse);
    _ubo.setData(UboOrder::SPECULAR * 4 * sizeof(GLfloat), _specular);
    _ubo.setData(UboOrder::PV * 4 * sizeof(GLfloat), Camera::getPView());
    const unsigned int floatOffset = (UboOrder::PV * 4 +16) * sizeof(GLfloat);
    _ubo.setData(floatOffset, getNear());
    _ubo.setData(floatOffset + sizeof(GLfloat), getFar());
    _ubo.setData(floatOffset + 2 * sizeof(GLfloat), _halfAngle);
}
