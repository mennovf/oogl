#include "texture.hpp"
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <functional>

Texture::Texture(GLenum target):
    GLWrapper(std::bind(glBindTexture, target, std::placeholders::_1), glGenTextures, glDeleteTextures),
    _target{ target },
    _size{}{
}

void Texture::setParameter(GLenum name, GLint value){
    auto guard = _res->scopedBind();
    glTexParameteri(_target, name, value);
}

GLuint Texture::getTexture() const{
    return _res->getID();
}

sf::Vector2u Texture::getSize() const{
    return _size;
}

sf::Image Texture::getImage(const std::string& fileName){
    sf::Image img;
    if (!img.loadFromFile(fileName)){
        std::cerr << "Error in texture with opening file: " << fileName << std::endl;
    }
    return img;
}


bool operator<(const Texture& lhs, const Texture& rhs){
    return lhs.getTexture() < rhs.getTexture();
}
