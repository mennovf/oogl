#ifndef GLRESOURCE_HPP
#define GLRESOURCE_HPP
#include <functional>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>

class GLResource{
public:
    class ScopedBind{
    public:
        ScopedBind(GLResource*);
        ScopedBind(ScopedBind&&);
        virtual ~ScopedBind();

    protected:
        GLResource* _res;
    };
    typedef std::function<void(GLuint)> BindFunc;
    typedef std::function<void(GLsizei, GLuint*)> GenFunc;
    typedef std::function<void(GLsizei, const GLuint*)> DelFunc;
    //typedef void (* BindFunc)(GLenum, GLuint);
    //typedef void (* GenFunc)(GLsizei, GLuint*);
    //typedef void (* DelFunc)(GLsizei, const GLuint*);

    GLResource();
    GLResource(BindFunc, GenFunc, DelFunc);
    virtual ~GLResource();

    void gen();
    GLuint getID();
    void setID(GLuint);

    void bind();
    void unbind();
    ScopedBind scopedBind();
protected:
    const BindFunc _bind_func;
    const GenFunc _gen_func;
    const DelFunc _delete_func;

    GLuint _id;
};



#endif /* end of include guard: GLRESOURCE_HPP */

