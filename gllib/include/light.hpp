#ifndef LIGHT_HPP
#define LIGHT_HPP
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "ubo.hpp"
#include "camera.hpp"

class Light : public Camera{
public:
    Light ();
    Light(float, glm::vec3, glm::vec3, glm::vec3);
    virtual ~Light () = default;

    void setHalfAngle(float);
    void setAmbient(glm::vec3);
    void setDiffuse(glm::vec3);
    void setSpecular(glm::vec3);

    float getHalfAngle() const;
    glm::vec3 getAmbient() const;
    glm::vec3 getDiffuse() const;
    glm::vec3 getSpecular() const;
    void updateUbo() const;
    Ubo& getUbo() const; 

protected:
    float _halfAngle; //This is the half-angle of the cone of light, Pi for an omni light
    glm::vec3 _ambient, _diffuse, _specular;

    mutable bool _uboCreated;
    mutable Ubo _ubo;
};

#endif /* end of include guard: LIGHT_HPP */

