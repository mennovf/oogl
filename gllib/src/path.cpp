#include "path.hpp"
#include <numeric>
#include <cmath>
#include <iostream>

Path::Path():
    _points{},
    _ts{},
    _t{0},
    _max{0}{
        calcMax();
    }

Path::Path (const std::vector<glm::vec3>& ps, const std::vector<float>& ts):
    _points(ps),
    _ts(ts),
    _t{0},
    _max{0}{
        calcMax();
    }

Path::Path (const std::vector<glm::vec3>& ps, float t):
    Path{ps, std::vector<float>(ps.size(), t)}{
}

void Path::update(float dt){
    if (_t < _max){
        _t = (_t + dt) <= _max ? _t+ dt : _max;
    }
}

glm::vec3 Path::getPosAlongPath() const{
    if (isDone()){
        return *(--_points.cend());
    }
    float passed = 0;
    unsigned int index = 0;
    for(unsigned int i = 0; i < _ts.size() - 1; ++i){
        if(passed + _ts[i] > _t){
            index = i;
            break;
        }
        passed += _ts[i];
    }

    float segT = (_t - passed) / _ts[index];
    return _points[index] + segT * (_points[index+1] - _points[index]);
}

void Path::reset(){
    _t = 0;
}


bool Path::isDone() const{
    return _t >= _max;
}

void Path::calcMax(){
    _max = std::accumulate(_ts.cbegin(), _ts.cend(), 0.f);
}
