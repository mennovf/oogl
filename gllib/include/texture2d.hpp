#ifndef TEXTURE2D_HPP
#define TEXTURE2D_HPP

#include "texture.hpp"
#include <string>
#include <memory>

class Texture2D : public Texture{
public:
    typedef std::shared_ptr<Texture2D> Ptr;
    Texture2D();
    Texture2D(const std::string&);
    virtual ~Texture2D() = default;

    virtual void loadFromFile(const std::string&) override;
    void genEmpty(sf::Vector2u, GLenum format = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE);

private:
    void genTexture(const sf::Uint8*, sf::Vector2u, GLenum format = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE);
};
#endif