#version 420
in vec3 pos_eye;
in vec3 normal_eye;
out vec4 frag_colour;
layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};

void main(){
    frag_colour = vec4(1);
}

