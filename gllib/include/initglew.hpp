#ifndef INITGLEW_HPP
#define INITGLEW_HPP
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
class InitGlew{
public:
    InitGlew(){
        glewInit();
    }
};
#endif
