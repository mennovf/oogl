#include "transform.hpp"

Transform::Transform(glm::vec3 p, glm::vec3 u, glm::vec3 f, glm::vec3 s, glm::vec3 ps):
    _forward{f},
    _up{u},
    _pos{p},
    _preScale{ps},
    _scale{s}{}
