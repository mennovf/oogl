#ifndef LIGHTSCREENSTATE_HPP
#define LIGHTSCREENSTATE_HPP

#include <state.hpp>
#include <memory>
#include "sceneNode.hpp"

class Texture;

class VisLight : public State{
public:
    VisLight(StateStack&, std::shared_ptr<Texture>, std::shared_ptr<Texture>);
    virtual ~VisLight() = default;
    virtual void render() override;

protected:
    SceneNode _root;
    std::shared_ptr<Texture> _color;
    std::shared_ptr<Texture> _depth;
};


#endif /* end of include guard: LIGHTSCREENSTATE_HPP */

