#include "engine.hpp"
#include "app.hpp"

Engine::Engine():
    StateStack{}{
        push(std::unique_ptr<State>(new App{*this}));
    }
