#ifndef CAMERA_HPP
#define CAMERA_HPP
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "sceneNode.hpp"

class Camera: public SceneNode{
public:
    Camera ();
    virtual ~Camera () = default;

    glm::mat4 getPerspective() const;
    glm::mat4 getView() const;
    glm::mat4 getPView() const;

    void setFar(float);
    float getFar() const;
    void setNear(float);
    float getNear() const;
    void setFov(float);
    float getFov() const;
    void setAspect(float);
    float getAspect() const;

protected:

    float _far, _near, _vFov, _aspect;
};


#endif /* end of include guard: CAMERA_HPP */

