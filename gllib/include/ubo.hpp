#ifndef UBO_HPP
#define UBO_HPP
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <vector>
#include <set>
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "glwrapper.hpp"

class Ubo : public GLWrapper{
public:
    Ubo();
    Ubo(unsigned int);
    Ubo(const std::vector<float>&);
    virtual ~Ubo() = default;

    //void bind();
    void setData(int, const std::vector<float>&);
    void setData(int, const glm::mat4&);
    void setData(int, const float);
    void setData(int, const glm::vec3);

    void bindBufferRange(int offset = 0, int size=-1);
    void bindBufferBase();

    unsigned int getBindingIndex() const;
    GLuint getUbo();

protected:
    static std::set<unsigned int> _usedBindingIndices;
    //GLuint _ubo;
    unsigned int _size;

    unsigned int _bindingIndex;
    unsigned int genBindingIndex();
};


#endif /* end of include guard: UBO_HPP */

