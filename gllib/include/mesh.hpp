#ifndef MESH_HPP
#define MESH_HPP
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <memory>
#include "glwrapper.hpp"

//Forward declaration
struct aiMesh;

class Mesh : public GLWrapper{
public:
    typedef std::shared_ptr<Mesh> Ptr;
    enum Attr{
        POS = 0,
        NORMAL,
        TEXCOORD,
        MAX
    };
    Mesh(GLenum mode = GL_TRIANGLES);
    Mesh(const std::string& file);
    virtual ~Mesh() = default;

    bool loadFromFile(const std::string& file);
    void setAttrFromMemory(Attr, const std::vector<float>&, unsigned int);
    GLuint getVao() const;
    int getVerts();

    GLenum getMode() const;
    void setMode(GLenum);

protected:
    void loadMesh(const aiMesh*);
    GLenum _mode;
    int _verts;
};


#endif /* end of include guard: MESH_HPP */

