#version 420
in vec3 pos_eye;
in vec3 normal_eye;
in vec4 inLightCoord[3];
in vec4 worldCoord;
in vec4 normal_world;
out vec4 frag_colour;
layout(binding = 0) uniform sampler2DShadow shadowTex;
layout(binding = 1) uniform samplerCube environment;
layout(std140) uniform CameraMatrices{
    mat4 V;
    mat4 P;
};

layout(std140) uniform Light{
    vec3 pos;
    vec3 dir;
    vec3 a;
    vec3 d;
    vec3 s;
    mat4 PV;
    float near;
    float far;
    float angle;
} light[3];

// surface reflectance
vec3 Ks = vec3 (0.1, 0.1, 0.1); 
vec3 Kd = vec3 (1.0, 0.5, 0.0);
vec3 Ka = vec3 (1.0, 1.0, 1.0); // fully reflect ambient light
float specular_exponent = 10.0; // specular 'power'

float shadowFactor(sampler2DShadow shadowMap, vec4 lightCoord){
    vec3 fromLight = normalize(worldCoord.xyz - light[0].pos);
    float angle = acos(dot(fromLight, normalize(light[0].dir)));
    float shadow = 0;
    if (angle < light[0].angle){
        vec3 shadowCoord = (lightCoord.xyz / lightCoord.w)*0.5 + vec3(0.5);
        //Add some bias to the depth to reduce acne
        shadowCoord.z *= 0.98;

        shadow = texture(shadowMap, shadowCoord).r;
    }
    return shadow;
}

vec3 phong_for_light(vec3 light_position_world, vec3 Ls, vec3 Ld, vec3 La){
    // ambient intensity
    vec3 Ia = La * Ka;

    vec3 light_position_eye = vec3 (V * vec4 (light_position_world, 1.0));
    vec3 distance_to_light_eye = light_position_eye - pos_eye;
    vec3 direction_to_light_eye = normalize (distance_to_light_eye);

    vec3 ray = (reflect(-normalize(worldCoord), normalize(normal_world))).xyz;
    vec3 Id = texture(environment, ray).rgb;

    // specular intensity
    vec3 reflection_eye = reflect (-direction_to_light_eye, normal_eye);
    vec3 surface_to_viewer_eye = normalize (-pos_eye);
    float dot_prod_specular = dot (reflection_eye, surface_to_viewer_eye);
    dot_prod_specular = max (dot_prod_specular, 0.0);
    float specular_factor = pow (dot_prod_specular, specular_exponent);
    vec3 Is = Ls * Ks * specular_factor; // final specular intensity

    return (Id + Is) + Ia;
}

void main(){
    frag_colour = vec4(phong_for_light(light[0].pos, light[0].s, light[0].d, light[0].a), 1.0);
}
